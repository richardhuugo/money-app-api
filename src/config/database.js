const mongoose = require('mongoose')
//remover warning
mongoose.Promise = global.Promise

module.exports = mongoose.connect('mongodb://localhost/mymoney')

mongoose.Error.messages.general.required = "O atributo '{PATH}' é obrigatório"
mongoose.Error.messages.Number.min = " o valor informado é menor que o limite exigido"
mongoose.Error.messages.Number.max = "o valor informado e maior que o permitido"