const express = require('express')

module.exports = function(server) {
    
    // url base para todas as rotas
    const router = express.Router()
    server.use('/api', router)

    // rotas de ciclo de pagamento
    const money = require('../api/service/moneyService')
    money.register(router, '/money')


}

