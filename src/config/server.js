const port = 3003

// middle efetuar o parse no corpo da requisicao 
const bodyParser = require('body-parser')
// cria instancia
const express = require('express')
//instancia express
const server = express()
const allowCors = require('./cors')
const queryInt = require('express-query-int')
//interceptar requisicao e e interpreta urlencode
server.use(bodyParser.urlencoded({extended:true}))
//parse quando for json
server.use(bodyParser.json())
server.use(allowCors)
server.use(queryInt())

server.listen(port, function(){
    console.log(`BACKEND  is running on port${port}.`)
})

module.exports = server
