const money = require('./money')
const errorHandler = require('../common/errorHandle')

money.methods(['get', 'post', 'put', 'delete'])
//validar inclusao e alteraco
money.updateOptions({new:true, runValidators:true})
money.after('post', errorHandler).after('put',errorHandler)


money.route('count', (req, res, next) =>{
    money.count((error, value)=>{
        if(error){
        res.status(500).json({errors: [error]})
    }else{
        res.json({value})
    }
    })
})  


money.route('summary', (req, res, next) => {
    money.aggregate({
        $project: {credit: {$sum: "$credits.value"}, debt:{$sum: "$debts.value"}}
    },{
        $group: {_id: null, credit:{$sum: "$credit"}, debit: {$sum: "$debt"}}
    },{
        $project: {_id:0, credit:1, debt:1}
    },(error, result ) =>{
        if(error){
            res.status(500).json({erro:[error]})
        } else {
            res.json(result[0] || {credt: 0, debt:0 })
        }
    })
})
module.exports = money